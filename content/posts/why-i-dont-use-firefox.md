+++
author = "TheHolyTachanka"
title = "Why i dont use firefox"
date = "2022-12-03"
description = "I explain why i dont use firefox"
tags = [
    "Firefox",
    "Chrome",
    "Security"
]
+++
# Introduction
Let me start by saying that I love FOSS software, the idea behind it, the community, and the software itself. I also love Firefox and want to see it succeed, but Mozilla has been screwing up a lot lately, so I decided to document it all. I will update this article whenever Mozilla screws up again. I never thought I would have to say this, but... If you want to use a bloat-less Firefox-based browser, try LibreWolf; if you don't mind a Chromium-based one, try Brave.

# Questionable features
- Recently adding ads to the URL bar
https://www.techradar.com/news/ads-have-even-invaded-the-firefox-url-bar-now
- EME DRM botnet downloaded by default

- Pocket (proprietary service, ads, promoted content)
https://en.m.wikipedia.org/wiki/Mozilla_Pocket#History
>The integration was controversial, as users displayed concerns for the direct integration of a proprietary service into an open source application, and that it could not be completely disabled without editing advanced settings, unlike third-party extensions.

- Automatically installing Mr. Robot adware through experimental extensions program
https://www.cnet.com/tech/services-and-software/mozilla-backpedals-after-mr-robot-firefox-misstep/

- Promotes Cloudflare DNS MitM botnet
https://digdeeper.club/articles/mozilla.xhtml

- Inferior fingerprinting protection and other leaks
https://bugzilla.mozilla.org/show_bug.cgi?id=1372288

- Breaking compatibility for old extensions
https://blog.mozilla.org/addons/2018/08/21/timeline-for-disabling-legacy-firefox-add-ons/

- Breaking add-on certificate signing, disabling NoScript on Tor browser and threatening millions of users
https://www.forbes.com/sites/kateoflahertyuk/2019/05/05/firefox-extensions-are-broken-heres-what-to-do/

- Breaking css UI customization features

- Breaking user modifiable options (most recent compact mode)
https://www.ghacks.net/2021/04/07/mozilla-decides-to-hide-compact-mode-in-firefox-for-new-users-but-keep-it-for-existing-ones/

- Removing about:config sorting and filtering, completely removed on Android
https://www.ghacks.net/2019/01/31/firefoxs-new-aboutconfig-page/

- Poor security, even with Fission they are years behind (especially on Android)
https://madaidans-insecurities.github.io/firefox-chromium.html

- Overall performance sucks and has not improved (loses in 90% of benchmarks)
https://www.phoronix.com/scan.php?page=news_item&px=Firefox-95-Chrome-97

# Articles/Posts
Im not going to copy-paste snippets from a couple of articles. instead you can just read them on your own

- Madaidans, security researcher \
https://madaidans-insecurities.github.io/firefox-chromium.html
- thegrugq, information security researcher \
https://medium.com/@thegrugq/tor-and-its-discontents-ef5164845908
- Kenn White, security researcher \
https://twitter.com/kennwhite/status/804142071133126656
- PaXTeam, developer of PaX \
https://archive.fo/9aBLk
- Daniel Micay, lead developer of GrapheneOS \
https://grapheneos.org/usage#web-browsing
- Matthew Garrett, Linux developer \
https://news.ycombinator.com/item?id=13800323
- Dan Guido, CEO of Trail of Bits \
https://news.ycombinator.com/item?id=13623735
- Theo de Raadt, lead developer of OpenBsd \
https://marc.info/?l=openbsd-misc&m=152872551609819&w=2
- Thomas Ptacek, co-founder of Latacora and Matasano Security \
https://twitter.com/tqbf/status/930807512609296384 \
https://twitter.com/tqbf/status/930860544927649792 \
https://twitter.com/tqbf/status/830511154950766595 
- qwertyoruiopz, iOS exploit developer \
https://twitter.com/qwertyoruiopz/status/805887567493271556 \
https://twitter.com/qwertyoruiopz/status/730704655748075520 
- John Wu, Android security engineer \
https://twitter.com/topjohnwu/status/1105739918444253184 \
https://twitter.com/topjohnwu/status/1455606288419733505 
- Chris Rohlf, security engineer \
https://twitter.com/chrisrohlf/status/1455549993536966671
- Matthew Green, cryptographer at Johns Hopkins University \
https://twitter.com/matthew_d_green/status/830488564672626690
- Bruno Keith, security researcher at Dataflow Security \
https://twitter.com/bkth_/status/1265971734777380865
- Niklas Baumstark, security researcher at Dataflow Security \
https://twitter.com/_niklasb/status/1131129708073107456
- The Tor Project investigating ways to harden the Tor Browser \
https://gitlab.torproject.org/tpo/applications/tor-browser/-/wikis/Hardening
- Alex Gaynor, former Firefox security engineer and sandboxing lead \
https://news.ycombinator.com/item?id=22342352


