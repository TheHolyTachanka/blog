+++
author = "TheHolyTachanka"
title = "Setting up Voidlinux for gaming."
date = "2022-12-02"
description = "A short guide on setting up voidlinux for gaming"
tags = [
    "VoidLinux",
    "Linux",
    "Gaming"
]
+++

## Enabling repositories
The first thing we need to do is enable the repositories
```
sudo xbps-install void-repo-{nonfree,multilib,multilib-nonfree}
```
Then simply update your system
```
sudo xbps-install -Su
```
## Installing video drivers 
Nvidia:
```
sudo xbps-install nvidia nvidia-libs-32bit vulkan-loader vulkan-loader-32bit
```
AMD:
```
sudo xbps-install linux-firmware-amd mesa-dri mesa-dri-32bit vulkan-loader vulkan-loader-32bit mesa-vaapi mesa-vdpau
```
Intel:
```
sudo xbps-install linux-firmware-intel mesa-dri mesa-dri-32bit mesa-vulkan-intel mesa-vulkan-intel-32bit vulkan-loader vulkan-loader-32bit
```
## Installing wine
```
sudo xbps-install -Su wine wine-32bit freetype-32bit libgcc-32bit
```
## Done
Your system should now be ready for gaming; simply install Steam or Lutris and begin gaming.

If I missed any dependencies, please contact me or leave a comment so that I can add them to the post.
