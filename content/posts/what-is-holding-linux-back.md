+++
author = "TheHolyTachanka"
title = "What is holding the linux desktop back?"
date = "2022-12-06"
description = "My rant on the linux desktop"
tags = [
    "Linux",
    "GNU"
]
+++



## Desktop environments
Enormous waste of efforts and resources to reimplement the same things all over again, trying to "win" against each other. Desktop environments are constantly shifting driven by the devs vision, but all the average user wants is one simple, reliable and consistent interface; not an alpha stage GUI experiment. The big challenge in Linux for the desktop is focusing enough attention of devs and contributor to bug fixes and small but critical improvements instead of new feature addition. Personally, I prefer GNOME because I believe they have done the best job of creating a consistent and good-looking UI.

## Standardization
The failure of freedesktop and Linux filesystem hierarchy standards etc in promoting a common layer of base libraries, tools and configurations between distros, so that an actual platform can be targeted by third party software vendors. See what steam has to do to run on all linux systems.

## A solution?
Focus on what is already made; don't try to reimplement the same things over and over; there are already too many distros and desktop environments; if you aren't offering something new or unique, don't make it.

This was my rant, hope you enjoyed.